## Configure the Critique and Review module

## Settigs form
Go to
Home >> Administration >> Configuration >> Content authoring >>
Critique and Review Module Settings.
Choose which Content Types can be reviewd and add review items. Review items are
points that reviwer needs to fill. You can change number of review items and
change items themself, to change it simply change its Title and Instructions.
You can also delete unwanted items and add new ones.

## Add the review form on to the content
Go to
Home >> Administration >> Structure >> Block layout
Add block to desired region, Sidebar would be a good choice so the Review block
can apper to the right of the main text. Content is good choice if you need
wider review form.  Click Place Block button and coose the
"Critique And Review Block".

### Restrict by role
Configure the block and restrict which roles can see
the block, good choice woul be to give only "Content Editor" the rights to see
this block, or even better create a Reviwer role.

### Restrict by page type
This block only make sense on the node body page, so restrict its appearance
elswere. Go to Pages tab and select "Show for the listed pages" and
enter /node/* in the Pages text area.

### Restrict by content type
This block may only make sense for certain content types, so restrict its
appearance on all content types. Go to Content Type and select desired type,
for example Article.
