<?php

namespace Drupal\critique_and_review\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;

/**
 * Provides a 'critique_and_review' block.
 *
 * @Block(
 *   id = "critique_and_review_block",
 *   admin_label = @Translation("Critique And Review Block"),
 *   category = @Translation("Custom Critique And Review Block")
 * )
 */
class CritiqueAndReviewBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    // empty block
    $build['markup'] = [
      '#type' => 'item',
      '#markup' => "",
      '#attributes' => [
        'id' => 'nicey-template-review-form-bottom',
        'class' => ['nicey-template-review-form-wrapper'],
      ],
      '#cache' => [
        'contexts' => ['user'],
        'tags' => [],
        'max-age' => 0,
      ],
    ];
    $userIsNodeCreator = FALSE;

    // Get current node.
    if($node = \Drupal::routeMatch()->getParameter('node')){
      // Get current node.
      if ($node instanceof NodeInterface) {
        // Get current user.
        $curent_user_id = \Drupal::currentUser()->id();
        if ($curent_user_id === $node->getOwnerId()) {
          $userIsNodeCreator = TRUE;
        }
      }
    } 
  
    if ($userIsNodeCreator) {

      $vid = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->getLatestRevisionId($node->id());

      // check if there are any reviews
      $hasReviews = FALSE;
      dump($node->getOwnerId());
      dump($curent_user_id);
      dump($node->id());
      dump($vid);
      $reviews = critique_and_review_get_reviwers($node->id(), $vid);
      foreach ($reviews as $review) {
        $hasReviews = TRUE;
      }
      dump($hasReviews);

      if($hasReviews) {
        $url = Url::fromRoute('entity.node.edit_form', ['node' => $node->id()]);
        $project_link = Link::fromTextAndUrl(t('Edit'), $url);
        $project_link = $project_link->toRenderable();
        $project_link['#attributes'] = ['class' => ['button', 'button-action', 'button--primary', 'button--small']];
        $build['markup'] = [
          '#type' => 'item',
          '#markup' => "You have reviews. Edit to view them.",
          '#attributes' => [
            'id' => 'nicey-template-review-form-bottom',
            'class' => ['nicey-template-review-form-wrapper'],
          ],
          '#cache' => [
            'contexts' => ['user'],
            'tags' => [],
            'max-age' => 0,
          ],
        ];
        $build['link'] = $project_link;
        $build['link']['#cache'] = [
          'contexts' => ['user'],
          'tags' => [],
          'max-age' => 0,
        ];
      } 
      
      if(!$hasReviews) {
        $build['markup'] = [
          '#type' => 'item',
          '#markup' => "You do not have any reviews yet.",
          '#attributes' => [
            'id' => 'nicey-template-review-form-bottom',
            'class' => ['nicey-template-review-form-wrapper'],
          ],
          '#cache' => [
            'contexts' => ['user'],
            'tags' => [],
            'max-age' => 0,
          ],
        ];
      }

    }

    if (!(\Drupal::currentUser()->isAnonymous()) && !$userIsNodeCreator ) {
      $build = [
        '#type' => 'markup',
        '#attributes' => ['id' => 'nicey-template-review-form', 'class' => ['nicey-template-review-form-wrapper']],
        'form' => \Drupal::formBuilder()->getForm('Drupal\critique_and_review\Form\TemplateCritiqueAndReviewForm'),
        '#cache' => [
          'contexts' => ['user'],
          'tags' => [],
          'max-age' => 0,
        ],
      ];
    }
    
    return $build;
  }

}
