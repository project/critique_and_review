<?php

namespace Drupal\critique_and_review\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a form to configure RSVP List module settings.
 */
class CritiqueAndReviewSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'critique_and_review_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'critique_and_review.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $types = node_type_get_names();
    $config = $this->config('critique_and_review.settings');

    $form['allowed_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('The content types to enable review collection for'),
      '#default_value' => $config->get('allowed_types'),
      '#options' => $types,
      '#description' => $this->t('On the specified node types, an review option will be available.'),
    ];
    $form['array_filter'] = ['#type' => 'value', '#value' => TRUE];

    if (NULL !== $config->get('intro_text')) {

      $intro_text = $config->get('intro_text');
    }
    else {
      $intro_text = "";
    }

    $form['intro_text'] = [
      '#type' => 'text_format',
      '#title' => 'Instroducton text',
      '#default_value' => $intro_text,
      '#format' => 'basic_html',
      '#description' => $this->t('Text that will appear at the top of the form, for exaple explain the revie process here. This is optional field'),

    ];

    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('<br><h3>Review items.</h3></br> Modify exsisting items and add new items.You can modify both the title and the instructions section.'),
    ];

    $titles = $config->get('review_items_titles');
    $instructions = $config->get('review_items_instructions');

    // If nothing set.
    if (!$titles) {
      $titles[] = "Add review item";
      $instructions[] = "";

    }
    $counter = 0;

    foreach ($titles as $title) {

      $form['container' . $counter] = [
        '#type' => 'details',
        '#title' => 'Review item ' . $counter + 1 . ': ' . $title,
        '#prefix' => '<div class="template-review-setings-form"',
        '#suffix'  => '</div>',
      ];

      $form['container' . $counter]['review_module_item_title' . $counter] = [
        '#type' => 'textfield',
        '#title' => 'Item Title',
        '#default_value'  => $title,
      ];
      $form['container' . $counter]['review_module_item_title' . $counter]['#attributes']['class'][] = "review-module-template-item-title";

      $form['container' . $counter]['review_module_item_instruction' . $counter] = [
        '#type' => 'text_format',
        '#title' => 'Instructions',
        '#format' => 'basic_html',
        '#default_value'  => $instructions[$counter],
      ];

      $form['container' . $counter]['delete_item' . $counter] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Delete this item'),
        '#description' => $this->t("Check if you wish this item to be deleted."),
      ];
      $counter++;
    }

    // Add another empty item used for add more.
    $form['container' . $counter] = [
      '#type' => 'details',
      '#title' => 'Add a new review item',
      '#suffix' => '<div class="template-review-setings-form"',
      '#prefix' => '</div>',
      '#description' => "Set title to add new review item. To add even more items save this form and repeat the process ",
    ];

    $form['container' . $counter]['review_module_item_title' . $counter] = [
      '#type' => 'textfield',
      '#title' => 'Item Title',
      '#default_value'  => "",
      '#description' => "Title is mandatory, leave this field empty only if you do not want to add a new item.",
    ];
    $form['container' . $counter]['review_module_item_title' . $counter]['#attributes']['class'][] = "review-module-template-item-title";

    $form['container' . $counter]['review_module_item_instruction' . $counter] = [
      '#type' => 'text_format',
      '#title' => 'Instructions. (optional)',
      '#format' => 'basic_html',
      '#default_value'  => "",
      '#description' => "Save to add another item.",
    ];

    $form['number_of_items'] = [
      '#type' => 'hidden',
      '#title' => 'Number of Items',
      '#value'  => $counter,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Allowed types.
    $allowed_types = array_filter($form_state->getValue('allowed_types'));
    sort($allowed_types);

    // Review items.
    $item_title = [];
    $item_instructions = [];
    $numberOfItems = $form_state->getValue('number_of_items');

    for ($i = 0; $i < $numberOfItems; $i++) {

      // If delete check box was not clicked save the item.
      if (!$form_state->getValue('delete_item' . $i)) {
        $item_title[] = $form_state->getValue('review_module_item_title' . $i);
        $item_instructions[] = $form_state->getValue('review_module_item_instruction' . $i)['value'];
      }
    }

    // If the last item was filled add it.
    if ($last_title = $form_state->getValue('review_module_item_title' . $numberOfItems)) {
      $item_title[] = $last_title;
      $item_instructions[] = $form_state->getValue('review_module_item_instruction' . $numberOfItems)['value'];
    }

    $saveConfig = $this->config('critique_and_review.settings');
    $saveConfig->set('allowed_types', $allowed_types);

    if ($intro_text = $form_state->getValue('intro_text')) {
      $saveConfig->set('intro_text', $intro_text['value']);
    }
    $saveConfig->set('review_items_titles', $item_title);
    $saveConfig->set('review_items_instructions', $item_instructions);
    $saveConfig->save();

    parent::submitForm($form, $form_state);
  }

}
