<?php

namespace Drupal\critique_and_review\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Template Form for Critique And Review.
 *
 * Each filed is predefined type of a review.
 */
class TemplateCritiqueAndReviewForm extends FormBase {

  /**
   * The current active database's master connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;
  /**
   * Node id.
   *
   * @var int
   */
  private $nid;

  /**
   * User id.
   *
   * @var int
   */
  private $uid;

  /**
   * Node version id.
   *
   * @var int
   */
  private $vid;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $this->database = \Drupal::database();
    $this->uid = \Drupal::currentUser()->id();
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      $this->nid = $node->nid->value;
      $this->vid = $node->vid->value;
    }
    else {
      $this->nid = NULL;
      $this->vid = NULL;
    }

    $config = $this->config('critique_and_review.settings');

    $form['description'] = [
      '#type' => 'item',
      '#markup' => $config->get('intro_text'),
    ];

    // Index nuber of a item to display on the form.
    $counter = 0;

    // Get template items from the config for the module, these are title
    // and description.
    $templateItems = $config->get('review_items_titles');
    $template_items_instructions = $config->get('review_items_instructions');

    // Get review items from the database. These are title
    // and value (actual review).
    $reviews = $this->get();

    // Loop trought the config review items and build form for these items
    // If value exsists for the item show it.
    foreach ($templateItems as $item) {
      $form[] = $this->printFormItem($counter, $item, $template_items_instructions, $reviews);
      $counter++;
    }

    // Are there any aditional review items in the databse, free style added by
    // user.
    foreach ($reviews as $review) {
      if (!in_array($review->review_title, $templateItems, TRUE)) {
        $form[] = $this->printFormItem($counter, $review->review_title, "", $review->review);
        $counter++;
      }

    }

    $form['container_add_more'] = [
      '#type' => 'details',
      '#title' => "Add more",
    ];

    $form['container_add_more']['item_add_more_title'] = [
      '#type' => 'textfield',
      '#title' => 'Item title',
      '#default_value'  => "",
      '#description'  => "Add your own template item",
    ];

    $form['container_add_more']['nicey_template_add_more_text'] = [
      '#type' => 'text_format',
      '#title' => "",
      '#format' => 'basic_html',
    ];

    $form['container_add_more']['helptext'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('To add even more items submit the form and new item will appear.'),
      '#attributes' => ['class' => ["review-module-template-helptext"]],
    ];

    // Add a submit button that handles the submission of the form.
    if ($this->nid) {

      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
        '#attributes' => ['class' => ["review-module-template-form-submit"]],
      ];

      // $form['#attached']['library'][] = 'review/review.form';
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  private function printFormItem($counter, $item, $template_items_instructions, $reviews) {

    $form['container' . $counter] = [
      '#type' => 'details',
      '#title' => $item,
    ];

    $form['item_title' . $counter] = [
      '#type' => 'hidden',
      '#title' => 'Item title',
      '#value'  => $item,
    ];

    // If we pasin array mean we pass all review.
    if (is_array($reviews)) {
      // If item alredy has review get it and display it.
      $body = $this->setInitialBody($item, $reviews);
    }
    // Pasing in string, so use it as review body.
    else {
      $body = $reviews;
    }

    if ($template_items_instructions) {
      $title_instructions = $template_items_instructions[$counter];
    }
    else {
      $title_instructions = '';
    }

    $form['container' . $counter]['nicey_template_text' . $counter] = [
      '#type' => 'text_format',
      '#title' => $title_instructions,
      '#format' => 'basic_html',
      '#default_value'  => $body,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'review_template_review_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->cleanValues()->getValues();

    foreach ($values as $key => $value) {

      if (str_starts_with($key, "nicey_template_text")) {

        $arr = explode('nicey_template_text', $key);
        $index = $arr[1];
        $item_title = $values['item_title' . $index];

        $this->set($item_title, $value["value"]);
      }
    }

    // If add more not empty.
    if ($values['item_add_more_title']) {
      $this->set($values['item_add_more_title'], $values['nicey_template_add_more_text']['value']);
    }
  }

  /**
   * Write review items to the database.
   */
  private function set($review_title, $body) {

    // If row already exsists update.
    $reviews = $this->get($review_title);

    $rowExsits = FALSE;

    foreach ($reviews as $review) {
      $rowExsits = TRUE;
    }

    if ($rowExsits) {
      // Already exsists so update.
      $this->database
        ->update('critique_and_review_reviews')
        ->fields([
          'review' => $body,
        ])
        ->condition('uid', $this->uid)
        ->condition('nid', $this->nid)
        ->condition('vid', $this->vid)
        ->condition('review_title', $review_title)
        ->execute();
    }

    else {
      // Doesn't exsist, so set.
      $fields = [
        'uid' => $this->uid,
        'nid' => $this->nid,
        'vid' => $this->vid,
        'review_title' => $review_title,
        'review' => $body,
      ];
      $this->database
        ->insert('critique_and_review_reviews')
        ->fields($fields)
        ->execute();
    }
  }

  /**
   * Read review items from the database.
   */
  private function get($review_title = NULL) {

    $query = $this->database->select('critique_and_review_reviews', 'u')
      ->fields('u')
      ->condition('u.uid', $this->uid)
      ->condition('u.nid', $this->nid)
      ->condition('u.vid', $this->vid);

    if ($review_title) {
      $query->condition('review_title', $review_title);
    }

    return $query->execute()->fetchAll();
  }

  /**
   * Find items.
   *
   * Loop trought all items for this node, user, and revision,
   * they are all in the $reviews variable,
   * if item exsists in the dabase, return it, else return null.
   */
  private function setInitialBody($text, $reviews) {

    if (!$reviews) {
      return NULL;
    }

    foreach ($reviews as $review) {
      if ($review->review_title === $text) {
        return $review->review;
      }
    }
    return NULL;
  }

}
